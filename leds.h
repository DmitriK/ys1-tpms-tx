#ifndef LEDS_H_
#define LEDS_H_

#define LED1          P1_1
#define LED_GREEN     P1_1
#define LED2          P1_2
#define LED_RED       P1_2
#define LED3          P1_3
#define LED_YELLOW    P1_3

#define LED LED_GREEN

#endif // LEDS_H_
