#include "./cc1111.h"
#include "./leds.h"

#include <stdbool.h>
#include <stdint.h>

#define TX_AMP_EN P2_0
#define RX_AMP_EN P2_4
#define AMP_BYPASS_EN P2_3
// #define nop() __asm nop __endasm;

#define SLEEP_TIME (15000)  // in ms

// f_ref = 24MHz
// f_carrier = f_ref / (2^16) * FREQ[23:0]
// 315052000Hz
#define FREQ_SETTING (860302)

// R_DATA = (256 + DRATE_M)*2^(DRATE_E) / (2^28)*f_ref
// 8192Hz
#define DATA_RATE_MANTISSA (102)
#define DATA_RATE_EXPONENT (8)

static const __xdata uint8_t wheel_data[4][15] = {
    {0x0F, 0x55, 0x55, 0x55, 0x5E, 0xAA, 0x99, 0x66, 0xAA, 0x59, 0x99, 0x96,
     0x6A, 0x56, 0x40},
    {0x0F, 0x55, 0x55, 0x55, 0x5E, 0xAA, 0x99, 0x66, 0xA9, 0x65, 0x99, 0x66,
     0x69, 0x55, 0x40},
    {0x0F, 0x55, 0x55, 0x55, 0x5E, 0xAA, 0x99, 0x66, 0xA9, 0x65, 0x56, 0x9A,
     0x69, 0xA5, 0x80},
    {0x0F, 0x55, 0x55, 0x55, 0x5E, 0xAA, 0x99, 0x66, 0xA9, 0x59, 0x6A, 0x6A,
     0x6A, 0x96, 0x40},
};

void clock_init(void) {
  uint8_t tmp;
  // SET UP CPU SPEED!  USE 26MHz for CC1110 and 24MHz for CC1111
  // Set the system clock source to HS XOSC and max CPU speed,
  SLEEP &= ~SLEEP_OSC_PD;
  while ((SLEEP & SLEEP_XOSC_STB) != SLEEP_XOSC_STB) {
  }
  CLKCON = (CLKCON & ~(CLKCON_CLKSPD_MASK | CLKCON_OSC_MASK)) | CLKCON_CLKSPD_1;
  while ((CLKCON & CLKCON_OSC_MASK) == CLKCON_OSC_RC) {
  }
  SLEEP |= SLEEP_OSC_PD;
  while ((SLEEP & SLEEP_XOSC_STB) != SLEEP_XOSC_STB) {
  }

  // Set sleep timer for wakeups every 15s, so all 4 sensors transmit within a
  // minute
  // t_event0 = 750/f_ref * EVENT0 * 2^(5*WOR_RES)
  WORCTRL |= 1;  // period is 1ms
  tmp = WORTIME0;
  while (tmp == WORTIME0) {
  }
  WOREVT1 = SLEEP_TIME >> 8;
  WOREVT0 = 0xFF & SLEEP_TIME;

  // Enabel sleep timer so it can act as a wakeup source
  STIF = 0;
  WORIRQ &= ~(0x1);
  STIE = 1;
  WORIRQ |= 0x10;

  // Enable interrupts globally
  EA = 1;
}

void init_RF(void) {
  // Setup interrupt
  // RFTXRXIE = 1;
  // Interrupt on underflow and packet completion
  RFIM = (uint8_t)(RFIM_IM_TXUNF | RFIM_IM_DONE);
  // RFIM = RFIM_IM_DONE;

  RFIF = 0;
  IEN2 |= IEN2_RFIE;

  // Setup autocal
  RF_MCSM0 |= RF_MCSM0_FS_AUTOCAL_FROM_IDLE;

  // No special packet treatment.
  RF_PKTCTRL0 = 0;

  // Init radio specs

  FREQ2 = FREQ_SETTING >> 16;
  FREQ1 = 0xFF & (FREQ_SETTING >> 8);
  FREQ0 = 0xFF & FREQ_SETTING;
  // VCO stuff. Unneeded?
  //     if (freq > FREQ_EDGE_900 and freq < FREQ_MID_900) or (freq >
  //     FREQ_EDGE_400 and freq < FREQ_MID_400) or (freq < FREQ_MID_300):
  //         # select low VCO
  //         radiocfg.fscal2 = 0x0A
  //     elif freq <1e9 and ((freq > FREQ_MID_900) or (freq > FREQ_MID_400) or
  //     (freq > FREQ_MID_300)):
  //         # select high VCO
  //         radiocfg.fscal2 = 0x2A

  // Set modulation and power
  RF_MDMCFG2 = RF_MDMCFG2_MOD_FORMAT_ASK_OOK;
  RF_PA_TABLE1 = 0x50;
  RF_PA_TABLE0 = 0;
  RF_FREND0 |= 0x1;

  // R_DATA = (256 + DRATE_M)*2^(DRATE_E) / (2^28)*f_ref
  // 8192Hz
  RF_MDMCFG3 = DATA_RATE_MANTISSA;
  RF_MDMCFG4 &= ~0xF;
  RF_MDMCFG4 |= DATA_RATE_EXPONENT;
}

void io_init(void) {
  // USB, LED1, LED2, and LED3
  P1DIR |= 0xf;
  // amplifer configuration pins
  P2DIR |= 0x19;
  // initial states
  LED1 = 0;
  LED2 = 0;
  LED3 = 0;
  TX_AMP_EN = 0;
  RX_AMP_EN = 0;
  AMP_BYPASS_EN = 1;
}

void transmit(__xdata uint8_t* buffer) {
  while (RF_MARCSTATE == RF_MARCSTATE_TX) {
  }

  // Put radio into tx state
  LED3 = 1;
  RFST = RFST_STX;

  for (int i = 0; i < PKTLEN; i += 1) {
    while (RFTXRXIF == 0) {
    }
    RFTXRXIF = 0;
    RFD = buffer[i];
    if (i == PKTLEN - 1) {
      LED3 = !LED3;
    }
  }

  LED3 = 0;
}

// void rfTxRxIntHandler(void) __interrupt RFTXRX_VECTOR  // interrupt handler
// should transmit or receive the next byte
// {
//     // Clear interrupt - this must be done *BEFORE* reading RFD
//     RFTXRXIF = 0;

//     if(RF_MARCSTATE == RF_MARCSTATE_TX)
//     {   // Transmit Byte

//         // rftxbuf is a pointer, not a static buffer, could be an array
//         RFD = tx_buf[tx_index];
//         tx_index += 1;
//         LED1 = !LED1;
//     }
// }

void rfIntHandler(void)
    __interrupt RF_VECTOR  // interrupt handler should trigger on rf events
{
  // which events trigger this interrupt is determined by RFIM (set in
  // init_RF()) note: S1CON should be cleared before handling the RFIF flags.
  // S1CON &= ~(S1CON_RFIF_0 | S1CON_RFIF_1);
  S1CON = 0;

  // if (RFIF & RFIF_IM_DONE) {
  //     LED3 = 0;
  //     transmitting = false;
  // }
  if (RFIF & RFIF_IM_TXUNF) {
    // If underflow indicate via LED
    LED_RED = 1;
  } else {
    // Sent ok, clear error
    LED_RED = 0;
  }

  RFIF = 0;
}

void TxMode(void) {
  RF_MCSM1 = RF_MCSM1_TXOFF_MODE_FSTXON;
  // TX_AMP_EN = 1;

  // Prep radio
  RFST = RFST_SFSTXON;
  while ((RF_MARCSTATE) != RF_MARCSTATE_FSTXON) {
  }
}

void IdleMode(void) {
  // TX_AMP_EN = 0;
  RF_MCSM1 = RF_MCSM1_OFF;

  RFST = RFST_SIDLE;
  while ((RF_MARCSTATE) != RF_MARCSTATE_IDLE) {
  }

  S1CON = 0;
  RFIF = 0;
}

void sleep() {
  uint8_t tmp;
  // Reset sleep timer and enter PM2
  WORCTRL |= 0x04;
  tmp = WORTIME0;
  while (tmp == WORTIME0) {
  }
  tmp = WORTIME0;
  while (tmp == WORTIME0) {
  }

  SLEEP |= SLEEP_MODE_PM2;
  NOP();
  NOP();
  NOP();

  if (SLEEP_MODE_MASK & SLEEP != SLEEP_MODE_PM0) {
    PCON |= PCON_IDLE;
  }
}

void main(void) {
  int wheel = 0;

  clock_init();
  io_init();
  init_RF();

  // Packet length is always the same, so set it once only.
  PKTLEN = sizeof(wheel_data[0]);

  while (true) {
    TxMode();
    for (int i = 0; i < 5; i += 1) {
      transmit(wheel_data[wheel]);
    }
    IdleMode();
    wheel = 0x3 & (wheel + 1);
    sleep();
    // Returning from sleep, wait for crystal to stabilize
    while ((SLEEP & SLEEP_XOSC_STB) != SLEEP_XOSC_STB) {
    }
  }
}

void stIrq(void) __interrupt ST_VECTOR {
  // Clear sleep mode bits as this ISR can wake us up
  SLEEP &= ~(SLEEP_MODE_MASK);
  // Clear flags too
  STIF = 0;
  WORIRQ &= ~(0x1);
}
