#
# CC Debugger - Example Payload
# Fergus Noble (c) 2011
#

CC = sdcc
CFLAGS = --model-small --opt-code-speed --std-sdcc11

# NOTE: code-loc should be the same as the value specified for
# USER_CODE_BASE in the bootloader!
LDFLAGS_FLASH = \
	--out-fmt-ihx \
	--code-loc 0x1400 --code-size 0x8000 \
	--xram-loc 0xf000 --xram-size 0x300 \
	--iram-size 0x100

ifdef DEBUG
CFLAGS += --debug
endif

BUILDDIR = ./build

SRC = main.c

# ADB=$(addprefix $(BUILDDIR)/, $(SRC:.c=.adb))
# ASM=$(addprefix $(BUILDDIR)/, $(SRC:.c=.asm))
# LNK=$(addprefix $(BUILDDIR)/, $(SRC:.c=.lnk))
# LST=$(addprefix $(BUILDDIR)/, $(SRC:.c=.lst))
REL=$(addprefix $(BUILDDIR)/, $(SRC:.c=.rel))
# RST=$(addprefix $(BUILDDIR)/, $(SRC:.c=.rst))
# SYM=$(addprefix $(BUILDDIR)/, $(SRC:.c=.sym))

PROGS=$(addprefix $(BUILDDIR)/, custom.hex)
# PCDB=$(addprefix $(BUILDDIR)/, $(PROGS:.hex=.cdb))
# PLNK=$(addprefix $(BUILDDIR)/, $(PROGS:.hex=.lnk))
# PMAP=$(addprefix $(BUILDDIR)/, $(PROGS:.hex=.map))
# PMEM=$(addprefix $(BUILDDIR)/, $(PROGS:.hex=.mem))
# PAOM=$(addprefix $(BUILDDIR)/, $(PROGS:.hex=))

all: $(PROGS)

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

$(REL) : $(SRC) $(BUILDDIR)
	$(CC) -c $(CFLAGS) -o$(REL) $<

$(BUILDDIR)/custom.ihx: $(REL) Makefile
	$(CC) $(LDFLAGS_FLASH) $(CFLAGS) -o $(BUILDDIR)/custom.ihx $(REL)

$(PROGS): $(BUILDDIR)/custom.ihx
	packihx < $(BUILDDIR)/custom.ihx > $(BUILDDIR)/custom.hex

clean:
	rm -f $(ADB) $(ASM) $(LNK) $(LST) $(REL) $(RST) $(SYM)
	rm -f $(PROGS) $(PCDB) $(PLNK) $(PMAP) $(PMEM) $(PAOM)
